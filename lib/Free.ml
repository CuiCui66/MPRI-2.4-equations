(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-32-33-34-37-39"]

open Functor

module Make (F : Functor) = struct
  module Base = struct
    type 'a t =
      | Return of 'a
      | Op of 'a t F.t

    let return a = Return a

    let rec bind m f = match m with
      | Return a -> f a
      | Op o -> Op (F.map (fun m -> bind m f) o)
  end

  module M = Monad.Expand (Base)
  include M
  open Base

  let op xs = Op (F.map return xs)

  type ('a, 'b) algebra =
    { return : 'a -> 'b
    ; op : 'b F.t -> 'b
    }

  let rec run alg m = match m with
    | Return a -> a |> alg.return
    | Op o -> o |> F.map (run alg) |> alg.op
end
