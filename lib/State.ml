(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-32-33-34-37-39"]

module Make (S : sig
  type t
end) =
struct
  module Signature = struct
    type 'a t = Get of (S.t -> 'a) | Set of S.t * (unit -> 'a)

    (* NYI *)
    let map f = function
      | Get f2 -> Get (fun s -> s |> f2 |> f)
      | Set (s,f2) -> Set (s, fun () -> () |> f2 |> f)
  end

  module FreeState = Free.Make (Signature)
  include FreeState

  (* Define [Signature] so that ['a FreeState.t] is isomorphic to the
          following type:

     <<<
          type 'a t =
            | Return of 'a
            | Get of unit * (S.t -> 'a t)
            | Set of S.t * (unit -> 'a t)
     >>>
  *)

  let get () = op (Get Fun.id)

  let set s = op (Set (s, Fun.id))

  let run (m : 'a t) =
    let op m s = match m with
      | Signature.Get f -> f s s
      | Signature.Set (s, f) -> f () s
    in
    run {return = Fun.const; op } m
end
