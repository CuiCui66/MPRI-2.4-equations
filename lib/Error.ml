(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-32-33-37-39"]

module Signature = struct
  type 'a t = exn

  let map f s = s
end

module Error = Free.Make (Signature)
include Error

let err e = op e

let run m = run {return = Fun.id; op = fun e -> raise e} m
